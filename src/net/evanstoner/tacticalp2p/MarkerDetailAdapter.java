package net.evanstoner.tacticalp2p;

import java.util.UUID;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MarkerDetailAdapter extends android.support.v4.widget.CursorAdapter {

	public MarkerDetailAdapter(Context context, Cursor cursor, int flags) {
		super(context, cursor, flags);
		// TODO Auto-generated constructor stub
	}
	
	public MarkerDetailAdapter(Context context, Cursor cursor, boolean autoRequery) {
		super(context, cursor, autoRequery);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView txtShortDesc = (TextView)view.findViewById(R.id.txtShortDesc);
		TextView txtLongDesc = (TextView)view.findViewById(R.id.txtLongDesc);
		TextView txtDistance = (TextView)view.findViewById(R.id.txtDistance);
		ImageView imgIcon = (ImageView)view.findViewById(R.id.imgIcon);
		txtShortDesc.setText(cursor.getString(cursor.getColumnIndex(TP2PSQLHelper.MARKERS_SHORT_DESC)));
		txtLongDesc.setText(cursor.getString(cursor.getColumnIndex(TP2PSQLHelper.MARKERS_LONG_DESC)));
		txtDistance.setText("??"); //TODO calculate distance
		view.setTag(UUID.fromString(cursor.getString(cursor.getColumnIndex(TP2PSQLHelper.MARKERS_ID)))); // tag is used to get marker ID on click
		imgIcon.setImageDrawable(ScenarioDefinition.getCurrent(context).getDrawableByMarkerType(context.getResources(), cursor.getInt(cursor.getColumnIndex(TP2PSQLHelper.MARKERS_TYPE))));
	}
 
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.row_marker_detail, parent, false);
		bindView(view, context, cursor);
		return view;
	}

}
