package net.evanstoner.tacticalp2p;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class MarkerTypePickerDialogFragment extends DialogFragment {
	
	public interface MarkerTypePickerListener {
		public void onMarkerTypeSelected(int id, String name);
	}
	
	MarkerTypePickerListener mListener;
	MarkerType[] mMarkers;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle(R.string.markerTypePicker_dialogTitle);
	    // TODO get a list of all possible types for the mission
	    mMarkers = ScenarioDefinition.getCurrent(HomeActivity.getContext()).getMarkers();
	    String[] markerNames = new String[mMarkers.length];
	    for (int i = 0; i < mMarkers.length; i++) {
	    	markerNames[i] = mMarkers[i].getName();
	    }
	    builder.setItems(markerNames, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   mListener.onMarkerTypeSelected(mMarkers[which].getID(), mMarkers[which].getName());
	               // The 'which' argument contains the index position
	               // of the selected item
	           }
	    });
	    return builder.create();
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the MarkerTypePickerListener so we can send events to the host
            mListener = (MarkerTypePickerListener)activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement MarkerTypePickerListener");
        }
    }

}
