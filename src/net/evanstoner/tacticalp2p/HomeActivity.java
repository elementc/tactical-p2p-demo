package net.evanstoner.tacticalp2p;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class HomeActivity extends Activity {
	private static Context context;
	public static Context getContext(){
		return context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		setContentView(R.layout.activity_home);
		ScenarioDefinition.dumpExampleToSharedPreferences(getApplicationContext());
		DeviceSettings.initializeUserID(getApplicationContext());
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
	
	public void btnCreateMission_click(View view) {
		Intent intent = new Intent(this, CreateMissionActivity.class);
		startActivity(intent);
	}
	
	public void btnContinueMission_click(View view) {
		Intent intent = new Intent(this, MissionStatusActivity.class);
		startActivity(intent);
	}
	
	public void btnBluetoothChat_click(View view){
		Intent intent = new Intent(this, BluetoothChat.class);
		startActivity(intent);
	
	}
	public void btnJoinMission_click(View view){
		//https://code.google.com/p/zxing/wiki/ScanningViaIntent
		//use that link to capture the result
		
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		startActivity(intent);
	}

}
