package net.evanstoner.tacticalp2p;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class DeviceSettings {
	private static UUID authorID;
	private static UUID currentMissionKey;
	
	/**
	 * the author's UUID 
	 * @param c a context object (getApplicationContext() works)
	 * @return the author's ID
	 */
	public static UUID getAuthorID(Context c){
		if (authorID == null)
			initializeUserID(c);
		return authorID;
	}
	
	/**
	 * a methdod to initialzie the app user id field.
	 * @param c a context object (getApplicationContext() works)
	 */
	public static void initializeUserID(Context c){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
		
		String authorIDString = preferences.getString("author_id", "");
		
		if (authorIDString.equals("")){
			//not stored.
			UUID id = UUID.randomUUID();
			
			Editor ed = preferences.edit();
			ed.putString("author_id", id.toString());
			ed.commit();
			
		}
		authorID = UUID.fromString(preferences.getString("author_id", ""));
		
				
		
	}

	public static UUID getCurrentMissionKey(){
		
		if (currentMissionKey == null)
			initializeCurrentMissionKey();
		return currentMissionKey;
			
	}
	
	public static void initializeCurrentMissionKey(){
		currentMissionKey = UUID.fromString("818b7b50-942c-11e2-9e96-0800200c9a66");
		//TODO: initialize from storage like in initializeUserID. 
		//TODO: integrate with create and join mission
	}
}
