package net.evanstoner.tacticalp2p;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MissionStatusActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mission_status);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mission_status, menu);
		return true;
	}
	
	public void btnViewMap_click(View view) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:28.064478,-80.624572?z=19"));
		startActivity(intent);
	}
	
	public void btnViewList_click(View view) {
		Intent intent = new Intent(this, ListViewActivity.class);
		startActivity(intent);
	}
	
	public void btnAuthor_click(View view) {
		Intent intent = new Intent(this, AuthorActivity.class);
		startActivity(intent);
	}
	
	public void btnNearbyDevices_click(View view) {
		Intent intent = new Intent(this, NearbyDevsActivity.class);
		startActivity(intent);
	}

}
